<h1>Techub Demo project

<h2>This repo is for techub demo

<h3>how to get github account


First, you must make a user account on Gitlab if you don't have one already. Be sure to choose a user ID that you are happy using for the rest of your professional career as a developer. GitHub is a very important tool, you will be using this account again in the future. Join gitlab here: [GITLAB.COM](https://gitlab.com/users/sign_in#register-pane)


<h1>Fork the Repository

Now that you have your own GitHub account you can fork this repository.
A fork creates a copy of a GitHub repository in your own GitHub account. Thus, you have permission to make changes to the content of your copied repository without ever changing the original one. At the time of your fork, you will copy all of the current contents and the two repositories are now independent. So if someone commits changes to the original repository, they will not affect your forked copy.

Fork the original repository [https://github.com/EEOB-BioData/Unix-Git-Exercise](https://github.com/EEOB-BioData/Unix-Git-Exercise) by clicking the Fork button at the upper right corner of the repository page.

![](http://www.picz.ge/img/s2/1912/23/9/9cae0f90eb4c.png)


This will take you to the GitHub page for your very own GitHub repository! It should have a URL like the one below (where *<your GitHub ID>* should be your new GitHub ID):



```
https://github.com/<your GitHub ID>/Unix-Git-Exercise
```

![Image of Yaktocat](https://octodex.github.com/images/yaktocat.png)


